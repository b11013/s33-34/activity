// load the express js module into our application and save it in a const variable called express
const express = require("express");

//creates an app that uses express and stores it as app . //server - app is the server
const app = express();

const port = 4000;

//middleware - handles the data streaming to be json. automatically parses it //when string of data comes, it will convert it automatically. no need for data step or end step 
app.use(express.json())

//mock data
let users = [{
        username: "BMadrigal",
        email: "fate@gmail.com",
        password: "dontAtMe"
    },
    {
        username: "Luisa",
        email: "stronggirl@gmail.com",
        password: "pressure"
    }
];

let items = [{
        name: "roses",
        price: 170,
        isActive: true
    },
    {
        name: "tulips",
        price: 250,
        isActive: true
    },
];


//app.get(<endpoint>, <function for req and res>)
app.get('/', (req, res) => {
    res.send("Hello from my first Express.js API")
});

app.get('/greeting', (req, res) => {
    res.send('Hello from Batch182-Garcia');
});

//retrieval of the mock database
app.get('/users', (req, res) => {

    //res.send already stringifies for you
    res.send(users);

    // res.json(users);
});

// POST Method
app.post('/users', (req, res) => {

    console.log(req.body); // result: {}

    // simulate creating a new user document
    let newUser = {
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    }

    // push newUser into users array
    users.push(newUser);
    console.log(users);

    // send the updated users array in the client
    res.send(users);
});

// DELETE Method
app.delete('/users', (req, res) => {

    users.pop();
    console.log(users);

    // send the updated users array
    res.send(users);
});

// PUT Method
// update user's password
// :index - wildcard
//url: localhost:4000/users/0
app.put('/users/:index', (req, res) => {

    // empty object ({}) before putting values in the request body
    console.log(req.body)

    // an object that contains the value of URL params (referring to the wildcard in the endpoint)
    console.log(req.params)

    // parseInt the value of the number coming from req.params
    // ['0'] turns into [0]
    //parsing into number
    let index = parseInt(r - eq.params.index);

    // users[0].password
    // pertains to the first object which BMadrigal
    // the updated password will be coming from the request body
    users[index].password = req.body.password;

    // send the updated user
    res.send(users[index]);
});
/*
	Mini-Activity: 5 mins

	>> endpoint: users/update/:index
	>> method: PUT

		>> Update a user's username
		>> Specify the user using the index in the params
		>> Put the updated username in request body
		>> Send the updated user as a response
		>> Test in Postman 
		>> Send your screenshots in Hangouts

*/
// S O L U T I O N

app.put('/users/update/:index', (req, res) => {

    let index = parseInt(req.params.index);

    users[index].username = req.body.username;

    res.send(users[index]);
});

//////activity

//GET
app.get('/items', (req, res) => {


    res.send(items);


});

// POST Method
app.post('/items', (req, res) => {

    console.log(req.body); // result: {}

    // simulate creating a new user document
    let newItems = {
        name: req.body.name,
        price: req.body.price,
        isActive: req.body.isActive
    }

    // push newUser into users array
    items.push(newItems);
    console.log(items);

    // send the updated users array in the client
    res.send(items);
});

// DELETE Method
app.delete('/items', (req, res) => {

    items.pop();
    console.log(items);

    // send the updated users array
    res.send(items);
});

// PUT Method
// update item's password
// :index - wildcard
//url: localhost:4000/items/0
app.put('/items/:index', (req, res) => {

    // empty object ({}) before putting values in the request body
    console.log(req.body)

    // an object that contains the value of URL params (referring to the wildcard in the endpoint)
    console.log(req.params)

    // parseInt the value of the number coming from req.params
    // ['0'] turns into [0]
    //parsing into number
    let index = parseInt(req.params.index);

    // users[0].password
    // pertains to the first object which BMadrigal
    // the updated password will be coming from the request body
    items[index].price = req.body.price;

    // send the updated user
    res.send(items[index]);
});




//port listener
app.listen(port, () => console.log(`Server is running at port ${port}`))